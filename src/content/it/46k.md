Ad una rapida occhiata, sembra che la mossa di nero sia una mossa illegale...

Board::problem-1

---

4 pietre nere sono in pericolo!

Cattura le pietre bianche prima di essere catturato!

Board::problem-2

---

Cattura le 2 pietre bianche a contatto con le pietre nere.

Qui non puoi catturare bianco con una sola non mossa.

Board::problem-3

---

Se riesci a catturare la pietra bianca segnata che separa le pietre nere, sarà
una partita facile.

Non cercare di catturare il nemico direttamente.

Board::problem-4

---

Perchè è importante catturare le pietre avversarie?

Il motivo principale è perchè grandi gruppi di pietre connesse sono molto più difficili
da uccidere e quindi, molto forti.

Questo è un aspetto molto importante del Go.

