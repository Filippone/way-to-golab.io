## Prima di iniziare

Ricorda le seguenti 3 regole. 

* Due giocatori (nero e bianco) posizionano sulla tavola (goban) una pietra a testa in
  ognuno dei loro turni.
* Una pietra deve essere posizionata sulle intersezioni delle linee orizzontali
  e verticali.
* Una volta che una pietra è stata posizionata, non può essere spostata, tuttavia
  sotto certe condizioni può essere rimossa.

Semplice, giusto?

Ora conosci metà delle regole del go. Cominciamo!

## L'obbiettivo del Go

L'obbiettivo del Go è di ottenere più punti dell'avversario.

Un modo per ottenere punti è **catturando** le pietre avversarie.

Le pietre che sono completamente circondate vengono rimosse dal goban e conservate
dall'avversario come prigioniere. Ogni prigioniero vale un punto.

## Catturare le pietre

Board::atari

Con una mossa in più, la pietra bianca sarà completamente circondata e catturata.

Le pietre che possono essere rimosse aggiungendo una sola mossa, vengono dette essere in **atari**.

---

Board::captured

La pietra bianca è ora circondata. Quindi...

---

Board::ponnuki

...viene rimossa dal goban.

---

Board::escape

Se fosse il turno di bianco, potrebbe "scappare" aggiungendo un'altra pietra.

