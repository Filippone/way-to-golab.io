Ora che hai visto che le pietre connesse diagonalmente possono essere tagliate,
possiamo vedere come difendersi dai tagli.

Board::defend-1

In questa situazione, bianco può tagliare le pietre nere. Tuttavia, nero può 
aggiungere una mossa per difendere il taglio, giocando quella che viene chiamata
una mossa di **connessione**.

---

Board::defend-2

In questa partita, nero ha giocato una **connessione diretta**. Non ci sono
possibilità per bianco di scollegare nero.

---

Qui invece, abbiamo una **connessione indiretta**, a volte chiamata connessione
sospesa (**hanging connection**).

Board::defend-3

Bianco può giocare una mossa legale per tagliare nero, ma nero può immediatamente
catturare la pietra quindi nero è, in sostanza, connesso.

Una connessione indiretta è, qualche volta, migliore di una connessione diretta. Se
puoi determinare quando ha più senso connettere indirettamente, non sei più
un principiante.

---

Board::defend-4

Qui sopra, nero ha due tagli. Può sembrare logico che nero possa difenderne
solo uno.

Tuttavia, la pietra segnata serve come connessione indiretta per entrambi i tagli.

Per via della sua forma, questa connessione è a volte chiamata **bocca di tigre**.

