By now, you may be understanding how to capture stones.  However, a game starts
with no stones on it.

Let's take a look at the standard 19x19 board.

Board::zones

To help make sense of the possibilities, we often refer to these 3 areas of the
board.

* The 4 blue areas are usually called the **corners**.
* The 4 green areas are usually called the **sides**.
* The red area is usually called the **middle**.

There aren't any actual borders here; we just name them to shorthand positions.

Where should we play first?

---

The object of Go is to surround as much *territory* as possible.  By that, we
mean you are trying to surround as many empty spaces as possible.

There are 3 groups in the following figure, each surrounding 9 spaces - or 9
**points**.

We call spaces that are completely surrounded **territory**.

Board::efficiency

Let's take a look at the number of stones needed by each player to surround
that many points.

* Corner - 6 stones
* Side - 9 stones
* Center - 12 stones

As you can see, it is more efficient to try to make territory in the corners
first, then the sides, then the middle.  As such, each player usually starts in
the corner.  It is rare to see the first moves in the middle.

---

The following is a game played by two professionals.  Only the first 20 moves
or so will be shown, but you should get a feel for how the beginning of a game
might progress.

Board::progame

You can see that the game progresses without any clashes between stones.

With about 20 moves played, What is the *balance of power*?

Board::power

The blue areas represent where black has *influence* whereas the green area
represents the area where white has *influence*.

As you can see, all corners and sides loosely belong to either black or white.
As the game progresses, either player may decide to *invade* the opposing
player's influence.

Furthermore, once the corners and sides are occupied, the middle becomes more
important.  If a player invests too heavily on the corners and sides, they may
lose out later.

These general strategies in the opening game are called **fuseki**.
