import React from 'react';
import {Goban} from 'react-go-board';
import godash from 'godash';
import {fromPairs, isString} from 'lodash';

export default function StaticBoard({black = [], white = [], size = 19, annotations = [], highlights = {}}) {
    const blackBoard = godash.placeStones(
        new godash.Board(size),
        black.map(godash.sgfPointToCoordinate),
        godash.BLACK,
    );
    const board = godash.placeStones(
        blackBoard,
        white.map(godash.sgfPointToCoordinate),
        godash.WHITE,
    );

    const coordinateAnnotations = annotations.map(godash.sgfPointToCoordinate);

    const newHighlights = [];
    for (const color in highlights) {
        newHighlights.push([
            color,
            highlights[color].map(coordinate =>
                isString(coordinate) ? godash.sgfPointToCoordinate(coordinate) : coordinate),
        ]);
    }

    return <div className='board'>
        <Goban board={board} annotations={coordinateAnnotations}
            highlights={fromPairs(newHighlights)} />
    </div>;
}
