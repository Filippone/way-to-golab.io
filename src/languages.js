export default [
    {
        name: 'English',
        locale: 'en',
    },
    {
        name: 'Italiano',
        locale: 'it',
    },
];
