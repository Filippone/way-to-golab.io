At first glance, it seems like black's move is illegal...

Board::problem-1

---

4 black stones are in danger!

Kill the white stones before being killed!

Board::problem-2

---

Capture the 2 white stones touching the black ones.

Here, you can't capture white with one move.

Board::problem-3

---

If you can capture the marked white stone that separates the black stones, it
will be an easy game.

Don't try to attack the enemy directly.

Board::problem-4

---

Why is it important to capture the opponent's stones?

The main reason is because larger connected groups of stones are much harder to
kill and therefore, much stronger.

This is very important aspect of Go.
