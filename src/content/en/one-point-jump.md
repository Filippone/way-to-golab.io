In this lesson, you will learn about moves that are played when black and white
are *not* directly attacking one another.

It is the time to play moves that strengthen your position, prepare for a
future battle, or make territory.

Board::one-point-jump

The marked move is called the **one point jump**, or **ikken tobi**.  The two
black stones are almost connected.  It is common to see such a move used in
many situations including attacking, defending, or escaping.

There is also a related go proverb expressing the versatility of this move.

*No one point jump is a bad move.*

Essentially, if you don't know where to play, find a one point jump and play
it.

---

Board::white-approach

Here is a look at using a one point jump as a defensive move if your opponent
were to approach.

---

Board::double-approach

If black were to ignore white's move, black's corner could be attacked from
both sides.
