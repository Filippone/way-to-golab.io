In certe posizioni, nero può dare a bianco due atari contemporaneamente.
Dato che bianco non può salvarli entrambi, nero può catturare almeno una pietra.

Questo è chiamato **doppio atari**

---

Bianco ha troppi punti deboli per chiudere nero nell'angolo in alto a sinistra.

Board::double-atari-1

---

Dove puoi dare doppio atari?

Board::double-atari-2
